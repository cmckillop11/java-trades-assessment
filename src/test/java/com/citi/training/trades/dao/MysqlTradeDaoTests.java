package com.citi.training.trades.dao;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.citi.training.trades.exceptions.TradeNotFoundException;
import com.citi.training.trades.model.Trade;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("h2")
public class MysqlTradeDaoTests {
	
	@Autowired
	MysqlTradeDao mysqlTradeDao;
	
	@Test
	@Transactional
	public void testCreateAndFindAll() {
		mysqlTradeDao.create(new Trade(-1, "Stock", 20.99, 10));
		assertTrue(mysqlTradeDao.findAll().size() >= 1);
	}
	
	@Test
	@Transactional
	public void testCreateAndFindById() {
		Trade trade = mysqlTradeDao.create(new Trade(-1, "Stock", 20.99, 10));
		assertThat(mysqlTradeDao.findById(trade.getId()).equals(trade));
	}
	
	@Test (expected = TradeNotFoundException.class)
	@Transactional 
	public void testCreateAndDeleteById() {
		mysqlTradeDao.create(new Trade(-1, "Stock", 20.99, 10));
		mysqlTradeDao.deleteById(1);
		assertEquals(mysqlTradeDao.findById(1), 1);
	}
}
