package com.citi.training.trades.rest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.citi.training.trades.model.Trade;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@Transactional
@ActiveProfiles("h2")
public class TradeControllerIntegrationTests {

	private static final Logger logger = LoggerFactory.getLogger(TradeControllerTests.class);

	@Autowired
	private TestRestTemplate restTemplate;
	
	@Test
    public void getTradeReturnsTrade() {
        restTemplate.postForEntity("/trades",
                                   new Trade(8, "Test Stock", 29.99, 10), Trade.class);

        ResponseEntity<List<Trade>> getAllResponse = restTemplate.exchange(
                                "/trades",
                                HttpMethod.GET,
                                null,
                                new ParameterizedTypeReference<List<Trade>>(){});

        logger.info("getAllTrades response: " + getAllResponse.getBody());

        assertEquals(HttpStatus.OK, getAllResponse.getStatusCode());
        assertTrue(getAllResponse.getBody().get(0).getStock().equals("Test Stock"));
        assertEquals(getAllResponse.getBody().get(0).getPrice(), 29.99, 0.0001);
        assertEquals(getAllResponse.getBody().get(0).getVolume(), 10);
        
        restTemplate.delete("/trades/" + getAllResponse.getBody().get(0).getId());
    }
}
