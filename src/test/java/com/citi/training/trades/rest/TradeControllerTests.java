package com.citi.training.trades.rest;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import com.citi.training.trades.model.Trade;
import com.citi.training.trades.service.TradeService;
import com.fasterxml.jackson.databind.ObjectMapper;


@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class TradeControllerTests {

	private static final Logger logger = LoggerFactory.getLogger(TradeControllerTests.class);
	
	@Autowired
	private ObjectMapper objectMapper;
	
	@Autowired
	private MockMvc mockMvc;
	
	@MockBean
	private TradeService mockTradeService;
	
	@Test
    public void findAllTradesReturnsList() throws Exception {
        when(mockTradeService.findAll()).thenReturn(new ArrayList<Trade>());
        MvcResult result = this.mockMvc.perform(get("/trades")).andExpect(status().isOk()).andExpect(jsonPath("$.size()").
        		isNumber()).andReturn();
        logger.info("Result from TradeService.findAll: " + result.getResponse().getContentAsString());
    }
	
	@Test
    public void createTradeReturnsCreated() throws Exception {
        Trade testTrade = new Trade(8, "Test Stock", 29.99, 10);
        this.mockMvc.perform(post("/trades").contentType(MediaType.APPLICATION_JSON)
        		.content(objectMapper.writeValueAsString(testTrade)))
        		.andExpect(status().isCreated()).andReturn();
        logger.info("Result from Create Trade");
    }
	
	@Test
    public void deleteTradeReturnsOK() throws Exception {
        MvcResult result = this.mockMvc.perform(delete("/trades/4")) .andExpect(status().isNoContent()).andReturn();
        logger.info("Result from TradeService.delete: " + result.getResponse().getContentAsString());
    }
	
	@Test
    public void getTradeByIdReturnsOK() throws Exception {
        Trade testTrade = new Trade(4, "Test Stock", 29.99, 10);

        when(mockTradeService.findById(testTrade.getId())).thenReturn(testTrade);

        MvcResult result = this.mockMvc.perform(get("/trades/4"))
                                       .andExpect(status().isOk()).andReturn();

        logger.info("Result from TradeService.getTrade: " +
                    result.getResponse().getContentAsString());
    }
}
