package com.citi.training.trades.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class TradesTests {
	
	private int testId = 44;
	private String testStock = "AAPL";
	private double testPrice = 20.99;
	private int testVolume = 10;
	
	@Test
	public void testTradeConstructor() {
		Trade testTrade = new Trade(testId, testStock, testPrice, testVolume);
		assertEquals(testId, testTrade.getId());
		assertEquals(testStock, testTrade.getStock());
		assertEquals(testPrice, testTrade.getPrice(), 0.001);
		assertEquals(testVolume, testTrade.getVolume());
	}
	
	@Test
	public void testTradetoString() {
		String testString = new Trade(testId, testStock, testPrice, testVolume).toString();
		assertTrue(testString.contains((new Integer(testId)).toString()));
		assertTrue(testString.contains(testStock));
		assertTrue(testString.contains(String.valueOf(testPrice)));
		assertTrue(testString.contains((new Integer(testVolume)).toString()));
	}
}
