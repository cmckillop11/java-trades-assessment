package com.citi.training.trades.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Component;

import com.citi.training.trades.exceptions.TradeNotFoundException;
import com.citi.training.trades.model.Trade;

/**
 * Class to execute all SQL statements 
 * 
 * @author Administrator
 *
 */
@Component 
public class MysqlTradeDao implements TradeDao{
	
	@Autowired
	JdbcTemplate tpl;
	
	/**
	 * Returns a list of all {@link com.citi.training.trades.model.Trade}s
	 */
	public List<Trade> findAll(){
		return tpl.query("SELECT * FROM trade", new TradeMapper());
	}

	/**
	 * Finds and returns all details of an {@link com.citi.training.trades.model.Trade} 
	 * by it's integer id 
	 * @param id The id of the trade to find 
	 */
	public Trade findById(int id) {
		List<Trade> trades = tpl.query("SELECT * FROM trade WHERE id=?", new Object[] {id}, 
				new TradeMapper());
		if(trades.size() <= 0) {
			throw new TradeNotFoundException("Trade with id: " + id + " not found");
			
		}
		return trades.get(0);
	}
	
	/**
	 * Creates an {@link com.citi.training.trades.model.Trade} 
	 * @param trade The trade to create
	 */
	public Trade create(Trade trade) {
		KeyHolder keyHolder = new GeneratedKeyHolder();
		tpl.update(new PreparedStatementCreator() {
			@Override
			public PreparedStatement createPreparedStatement(Connection connection) throws SQLException{
				PreparedStatement ps = connection.prepareStatement("INSERT INTO trade (stock, price, volume) VALUES (?, ?, ?)",
						Statement.RETURN_GENERATED_KEYS);
				ps.setString(1, trade.getStock());
				ps.setDouble(2, trade.getPrice());
				ps.setInt(3, trade.getVolume());
				return ps;
			}
		}, keyHolder);
		trade.setId(keyHolder.getKey().intValue());
		return trade;
	}
	
	/** 
	 * Deletes an {@link com.citi.training.trades.model.Trade} by it's integer id
	 * @param id The id of the trade to delete 
	 */
	public void deleteById(int id) {
		findById(id);
		tpl.update("DELETE FROM trade WHERE id=?", id);
	}
	
	public static final class TradeMapper implements RowMapper<Trade>{
		public Trade mapRow(ResultSet rs, int rowNum) throws SQLException{
			return new Trade(rs.getInt("id"), rs.getString("stock"), rs.getDouble("price"), rs.getInt("volume"));
		}
	}
}
